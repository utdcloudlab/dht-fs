package hdfs.server.namenode;

import nio.server.IProcessor;
import nio.server.TCPServer;
import dhtfs.core.Configuration;

public class NameNode {

	public static void main(String[] args) throws Exception{
		String confFile = "conf/masternode.conf";
		if (args.length >= 1) {
			confFile = args[0];
		}
		Configuration conf = new Configuration();
		conf.initialize(confFile);
		IProcessor processor = new NameNodeRequestProcessor();
		processor.initialize(conf);
		int port = Integer.parseInt(conf.getProperty("port"));
		TCPServer server = new TCPServer(port, processor);
		server.listen();
	}
}
