package dhtfs.core.def;

import java.io.IOException;

import dhtfs.core.Configuration;
import dhtfs.core.DhtPath;

/**
 * @author Yinzi Chen
 * @date May 6, 2014
 */
public class AbstractFileSystem implements IFileSystem {

    /*
     * (non-Javadoc)
     * 
     * @see dhtfs.core.def.IFileSystem#initialize(dhtfs.core.Configuration)
     */
    @Override
    public void initialize(Configuration conf) throws IOException {
        // TODO Auto-generated method stub

    }

    /*
     * (non-Javadoc)
     * 
     * @see dhtfs.core.def.IFileSystem#create(dhtfs.core.DhtPath)
     */
    @Override
    public IFile create(DhtPath path) throws IOException {
        // TODO Auto-generated method stub
        return null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see dhtfs.core.def.IFileSystem#open(dhtfs.core.DhtPath)
     */
    @Override
    public IFile open(DhtPath path) throws IOException {
        // TODO Auto-generated method stub
        return null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see dhtfs.core.def.IFileSystem#open(dhtfs.core.DhtPath, int)
     */
    @Override
    public IFile open(DhtPath path, int mode) throws IOException {
        // TODO Auto-generated method stub
        return null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see dhtfs.core.def.IFileSystem#delete(dhtfs.core.DhtPath)
     */
    @Override
    public void delete(DhtPath path) throws IOException {
        // TODO Auto-generated method stub

    }

    /*
     * (non-Javadoc)
     * 
     * @see dhtfs.core.def.IFileSystem#rename(dhtfs.core.DhtPath,
     * dhtfs.core.DhtPath)
     */
    @Override
    public void rename(DhtPath srcPath, DhtPath dstPath) throws IOException {
        // TODO Auto-generated method stub

    }

    /*
     * (non-Javadoc)
     * 
     * @see dhtfs.core.def.IFileSystem#copy(dhtfs.core.DhtPath,
     * dhtfs.core.DhtPath)
     */
    @Override
    public void copy(DhtPath srcPath, DhtPath dstPath) throws IOException {
        // TODO Auto-generated method stub

    }

    /*
     * (non-Javadoc)
     * 
     * @see dhtfs.core.def.IFileSystem#copyFromLocal(dhtfs.core.DhtPath,
     * dhtfs.core.DhtPath)
     */
    @Override
    public void copyFromLocal(DhtPath srcPath, DhtPath dstPath)
            throws IOException {
        // TODO Auto-generated method stub

    }

    /*
     * (non-Javadoc)
     * 
     * @see dhtfs.core.def.IFileSystem#copyToLocal(dhtfs.core.DhtPath,
     * dhtfs.core.DhtPath)
     */
    @Override
    public void copyToLocal(DhtPath srcPath, DhtPath dstPath)
            throws IOException {
        // TODO Auto-generated method stub

    }

    /*
     * (non-Javadoc)
     * 
     * @see dhtfs.core.def.IFileSystem#mkdir(dhtfs.core.DhtPath)
     */
    @Override
    public void mkdir(DhtPath path) throws IOException {
        // TODO Auto-generated method stub

    }

    /*
     * (non-Javadoc)
     * 
     * @see dhtfs.core.def.IFileSystem#rmdir(dhtfs.core.DhtPath, boolean)
     */
    @Override
    public void rmdir(DhtPath path, boolean recursive) throws IOException {
        // TODO Auto-generated method stub

    }

    /*
     * (non-Javadoc)
     * 
     * @see dhtfs.core.def.IFileSystem#listStatus(dhtfs.core.DhtPath)
     */
    @Override
    public void listStatus(DhtPath path) throws IOException {
        // TODO Auto-generated method stub

    }

    /*
     * (non-Javadoc)
     * 
     * @see dhtfs.core.def.IFileSystem#isDirectory(dhtfs.core.DhtPath)
     */
    @Override
    public boolean isDirectory(DhtPath path) throws IOException {
        // TODO Auto-generated method stub
        return false;
    }

    /*
     * (non-Javadoc)
     * 
     * @see dhtfs.core.def.IFileSystem#isFile(dhtfs.core.DhtPath)
     */
    @Override
    public boolean isFile(DhtPath path) throws IOException {
        // TODO Auto-generated method stub
        return false;
    }

    /*
     * (non-Javadoc)
     * 
     * @see dhtfs.core.def.IFileSystem#exists(dhtfs.core.DhtPath)
     */
    @Override
    public boolean exists(DhtPath path) throws IOException {
        // TODO Auto-generated method stub
        return false;
    }

}
