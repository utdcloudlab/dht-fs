package dhtfs.core.def;

/**
 * @author Yinzi Chen
 * @date May 6, 2014
 */
public interface IIDAssigner {

    public String generateUID();

}
