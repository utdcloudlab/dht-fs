package dhtfs.core;

import java.util.UUID;

import dhtfs.core.def.IIDAssigner;

/**
 * @author Yinzi Chen
 * @date May 6, 2014
 */
public class BlockNameAssigner implements IIDAssigner {

    /*
     * (non-Javadoc)
     * 
     * @see dhtfs.core.def.INameAssigner#generateUID()
     */
    @Override
    public String generateUID() {
        // TODO
        return UUID.randomUUID().toString();
    }
}
