package dhtfs.server.masternode;

import java.io.IOException;

import nio.client.TCPClient;
import nio.protocol.ProtocolReq;
import nio.protocol.ProtocolResp;
import nio.protocol.ReqType;
import nio.protocol.RespType;
import nio.protocol.table.JoinReq;
import nio.protocol.table.JoinResp;
import nio.protocol.table.TableReq;
import nio.protocol.table.TableResp;
import nio.server.ConnectionInfo;
import nio.server.IProcessor;
import dhtfs.core.Configuration;
import dhtfs.core.GeometryLocation;
import dhtfs.core.table.PhysicalNode;
import dhtfs.core.table.RouteTable;

public class MasterRequestProcessor implements IProcessor {

	protected Configuration conf;
	protected TCPClient client;
	protected RouteTable table;

    @Override
	public void initialize(Configuration config) throws IOException {
		conf = config;
		client = new TCPClient();
		try {
			table = (RouteTable) RouteTable.loadMeta(config
					.getProperty("imgFile"));
		} catch (IOException e) {
			table = new RouteTable();
			table.initialize(config);
		}
	}

	@Override
	public ProtocolResp process(ConnectionInfo info, ProtocolReq req) {
		ReqType reqType = req.getRequestType();
		System.out.println(reqType);
		ProtocolResp resp = null;
		switch (reqType) {
		case JOIN:
			resp = handleJoin(info, (JoinReq) req);
			break;
		case TABLE:
			resp = handleTableReq(info, (TableReq) req);
			break;
		default:
			;
		}
		resp.setrId(req.getrId());
		return resp;
	}

	public ProtocolResp handleJoin(ConnectionInfo info, JoinReq req) {
		JoinResp resp = new JoinResp(RespType.OK);
		PhysicalNode joinNode = new PhysicalNode(info.getIp(), req.getPort());
		GeometryLocation location = req.getLocation();
		joinNode.setLocation(location);
		table.join(joinNode);
		resp.setTable(table);
		resp.setLocal(joinNode);
		return resp;
	}

	public ProtocolResp handleTableReq(ConnectionInfo info, TableReq req) {
		TableResp resp = new TableResp(RespType.OK);
		resp.setTable(table);
		return resp;
	}
}
