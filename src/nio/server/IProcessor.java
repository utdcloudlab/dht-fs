package nio.server;

import java.io.IOException;

import dhtfs.core.Configuration;
import nio.protocol.ProtocolReq;
import nio.protocol.ProtocolResp;

public interface IProcessor {

    public ProtocolResp process(ConnectionInfo info, ProtocolReq req);

    public void initialize(Configuration config) throws IOException;

}
