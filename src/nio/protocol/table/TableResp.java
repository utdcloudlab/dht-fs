package nio.protocol.table;

import nio.protocol.ProtocolResp;
import nio.protocol.RespType;
import dhtfs.core.table.RouteTable;

public class TableResp extends ProtocolResp {
	private static final long serialVersionUID = 1L;

	private RouteTable table;

	public TableResp(RespType responseType) {
		super(responseType);
	}

	public TableResp(int rId, RespType responseType) {
		super(rId, responseType);
	}

	public RouteTable getTable() {
		return table;
	}

	public void setTable(RouteTable table) {
		this.table = table;
	}

}
