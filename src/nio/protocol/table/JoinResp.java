package nio.protocol.table;

import nio.protocol.ProtocolResp;
import nio.protocol.RespType;
import dhtfs.core.table.PhysicalNode;
import dhtfs.core.table.RouteTable;

public class JoinResp extends ProtocolResp {

	private static final long serialVersionUID = 1L;

	private RouteTable table;

	private PhysicalNode local;

	public JoinResp(RespType responseType) {
		super(responseType);
	}

	public JoinResp(int rId, RespType responseType) {
		super(rId, responseType);
	}

	public RouteTable getTable() {
		return table;
	}

	public void setTable(RouteTable table) {
		this.table = table;
	}

	public PhysicalNode getLocal() {
		return local;
	}

	public void setLocal(PhysicalNode local) {
		this.local = local;
	}

}
