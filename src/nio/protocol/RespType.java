package nio.protocol;

public enum RespType {
    OK, FWD, IOERROR, INVALID_PARAM, UNRECOGNIZE
}
